//
//  DataBaseHelper.swift
//  Marvel Characters
//
//  Created by Admin on 11/12/20.
//  Copyright © 2020 Praveen. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class DataBaseHelper {
static let shareInstance = DataBaseHelper()
let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
func saveImage(data: Data,name:String,desc:String) {
    let marvelInstance = Marvel(context: context)
            marvelInstance.name = name
            marvelInstance.details = desc
            marvelInstance.image = data
    do {
    try context.save()
    print("Data is saved")
    } catch {
    print(error.localizedDescription)
          }
   }
func fetchDate() -> [Marvel] {
    var fetchingImage = [Marvel]()
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Marvel")
    do {
    fetchingImage = try context.fetch(fetchRequest) as! [Marvel]
    } catch {
    print("Error while fetching the image")
    }
    return fetchingImage
    }
    func deleteData(index: Int){
      var fetchingImage = [Marvel]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Marvel")
        do {
        fetchingImage = try context.fetch(fetchRequest) as! [Marvel]
        } catch {
        print("Error while fetching the image")
        }
        context.delete(fetchingImage[index])
         do {
           try context.save()
           print("Data is saved")
           } catch {
           print(error.localizedDescription)
                 }
        }
    
}

