//
//  ViewController.swift
//  Marvel Characters
//
//  Created by Admin on 10/12/20.
//  Copyright © 2020 Praveen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var charactersArray:[Marvel] = []
    var isExpanded = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       setDefaultList()
    }


}
extension ViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  if isExpanded == false{
            return charactersArray.count
//        }
//        else{
//            return 1
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "MarvelListTableViewCell", for: indexPath) as? MarvelListTableViewCell
        cell?.nameLabel.text = charactersArray[indexPath.row].name
        cell?.descLabel.text = charactersArray[indexPath.row].details
        cell?.charImage.image = UIImage(data: charactersArray[indexPath.row].image ?? Data())
        return cell ?? UITableViewCell()
        
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            DataBaseHelper.shareInstance.deleteData(index: indexPath.row)
           charactersArray = DataBaseHelper.shareInstance.fetchDate()
            tableView.reloadData()
        }
    }
    
    func setDefaultList(){
        
        if DataBaseHelper.shareInstance.fetchDate().count == 0{
        // add default Data
        
        DataBaseHelper.shareInstance.saveImage(data:  #imageLiteral(resourceName: "Spider Man").pngData()!, name: "Spider Man", desc: "Bitten by a radioactive spider, Peter Parker’s arachnid abilities give him amazing powers he uses to help others, while his personal life continues to offer plenty of obstacles.")
        DataBaseHelper.shareInstance.saveImage(data:  #imageLiteral(resourceName: "Iron Man").pngData()!, name: "Iron Man", desc: "Genius. Billionaire. Philanthropist. Tony Stark's confidence is only matched by his high-flying abilities as the hero called Iron Man.")
        DataBaseHelper.shareInstance.saveImage(data:  #imageLiteral(resourceName: "Thanos").pngData()!, name: "Thanos", desc: "The Mad Titan Thanos quests across the universe in search of the Infinity Stones, intending to use their limitless power for shocking purposes.")
        DataBaseHelper.shareInstance.saveImage(data:  #imageLiteral(resourceName: "Captain America").pngData()!, name: "Captain America", desc: "Recipient of the Super-Soldier serum, World War II hero Steve Rogers fights for American ideals as one of the world’s mightiest heroes and the leader of the Avengers.")
        }
        
            charactersArray = DataBaseHelper.shareInstance.fetchDate()
            tableView.reloadData()
               
    }
    
}

