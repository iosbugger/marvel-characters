//
//  AddNewCharacterViewController.swift
//  Marvel Characters
//
//  Created by Admin on 11/12/20.
//  Copyright © 2020 Praveen. All rights reserved.
//

import UIKit

class AddNewCharacterViewController: UIViewController{
    @IBOutlet weak var charImageView: UIImageView!
    @IBOutlet weak var descTaxtView: UITextView!
    @IBOutlet weak var nameLabel: UITextField!
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
       // charImageView.image = UIImage(named: "america")
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addPhoto))
        imagePicker.delegate = self
        charImageView.isUserInteractionEnabled = true
        charImageView.addGestureRecognizer(gesture)
        descTaxtView.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    @objc func addPhoto(){
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
        
        //self.presentModalViewController(imagePicker, animated: true)
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func saveButtonAction(_ sender: Any) {
      
        DataBaseHelper.shareInstance.saveImage(data: self.charImageView.image!.pngData()!, name: nameLabel.text!, desc: descTaxtView.text!)
        self.dismiss(animated: true, completion: nil)
      
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddNewCharacterViewController:UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        charImageView.image  = tempImage

        self.dismiss(animated: true, completion: nil)
    }
    private func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info:NSDictionary!) {
        

    }

    private func imagePickerControllerDidCancel(picker: UIImagePickerController!) {

        self.dismiss(animated: true, completion: nil)
    }
}
